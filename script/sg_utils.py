import tempfile
from pysemigroup.utils import lcm, save, load
import networkx as nx
import sys, copy
import collections
from chaines import *

def emptyAutomaton(letters):
    d = {}
    for x in letters:
        d[(0,x)] = [0]
    return Automaton(d,[0],[])

def weak_inverse(M):
    E = set(M)
    result = []
    while len(E)>0:
        x = M.pop_J_maximal(E)
        J = M.J_class_of_element(x)
        E = E.difference(J)

        for y in J:
            for z in J:
                if not (y==z) and M(y+z+y) == y and M(z+y+z) == z and not (M(y+z) in [z,y] or M(z+y) in [z,y]):
                    result.append((str(y),str(z)))
    return result     

def J_minimal(M):
    T = M._get_J_topological_sort()
    return T[len(T)-1]

def is_W(M, reverse=False):
    A = M._automaton
    O = nx.transitive_closure(nx.DiGraph(list(computeTransitionOrderedMonoid(A))))       
    if reverse:
        O = O.reverse()
    WI = weak_inverse(M)    
    for e in WI:
        N = SubMonoid(M,list(e))
        e = (N((e[0],)),N((e[1],)))
        E = J_minimal(N)
        for z in E:           
            x = N(e[0]+e[1])
            y = N.idempotent_power(e[0]+e[1]+z+e[0]+e[1])
            if not (x==y) and (not (M(str(y)),M(str(x))) in O.edges()):
                return False
    return True

def is_dual_W(M):
    return is_W(M, reverse=True)
            
def stable_semigroup(M):
    S = M._stable()[0]
    d = {}
    for x in S:
        for y in S:
            d[(x,(y,))] = [M(x+y)]
    return TransitionSemiGroup(Automaton(d,[0],[0]))        

def computeOrderedAutomata(A):
    Order = set()
    Aut = {}
    for p in A._states:
        Aut[p] = Automaton(A._transitions,[p],A._final_states)                           
    for p in A._states:
        for q in A._states:
            B = Aut[p]-Aut[q]
            if (not B.is_finite_state_reachable()):
                Order.add((q,p))  
    return Order

def computeTransitionOrderedMonoid(A):
    A = complete(A)
    M = TransitionSemiGroup(A)
    AutomataOrder = computeOrderedAutomata(A)
    MonoidOrder = set()
    for u in M:
        fu = M._automaton.word_to_transitions(u)    
        for v in M:
            if (u != v):
                fv = M._automaton.word_to_transitions(v)
                ordered_pair = True
                for q in A._states:                    
                    if (fu[q],fv[q]) not in AutomataOrder:
                        ordered_pair = False
                if ordered_pair:
                    MonoidOrder.add((u,v))       
    return MonoidOrder                
def graphvizSyntacticOrderedMonoid(L):
    return graphvizTransitionOrderedMonoid(L.automaton_minimal_deterministic())
def graphvizTransitionOrderedMonoid(A):    
    Order = computeTransitionOrderedMonoid(A)
    M = TransitionSemiGroup(A)    
    G = nx.algorithms.dag.transitive_reduction(nx.DiGraph(list(Order)))
    #SCCs = nx.strongly_connected_components(G)
    #LG = [G.subgraph(C) for C in SCCs]
    result = []
    E = M.idempotents()
    GGraphViz = "digraph {\n"
    nodes = ""
    for u in G.nodes():
        nodes += '"'+str(u)+'"'
        if u in E:
            nodes += '[fill=white fontcolor = red]'
            nodes += ';\n'
            edges = ""    
    for e in G.edges():
        edges += '"'+str(e[0])+'"->"'+str(e[1])+'";\n'
        GGraphViz =  'digraph {\n'+ nodes + edges+'}'
        GGraphViz = GGraphViz.replace('""','"1"')
    result.append(GGraphViz)
    return result        
def getLocalSemiGroups(M):
    aut = M._automaton
    N = TransitionSemiGroup(aut, monoid=False)
    E = set(N.idempotents())
    L = []
    
    for x in list(E):
        if (len(x) == 0):
            E.remove(x)

    for e in E:
        sub = set([N(e+x+e) for x in N])
        L.append(N.sub_semigroup_generated(sub))
    return L

def getMeSemiGroup(M):
    import networkx as nx
    G = nx.transitive_closure(M.cayley_graph(orientation="left_right").reverse())
    # compute the upset instead of the ideal. Should be a sg method.
    
    E = set(M.idempotents())
    for e in E:
        if not len(e):
            continue
        Me = M.sub_monoid_generated(G.neighbors(e))
        e = Me((e,))
        yield Me.sub_semigroup_generated([Me(e+m+e) for m in Me])

def is_LDA(M):
    for x in getLocalSemiGroups(M):
        if not(is_DA(x)):
            return False
    return True

def is_MeDA(M):
    for S in getMeSemiGroup(M):
        if not is_DA(S):
            return False
    return True

def is_LSL(M):
    for x in getLocalSemiGroups(M):
        if not(is_SL(x)):
            return False
    return True

def is_LJ(M):
    for x in getLocalSemiGroups(M):
        if not(is_J(x)):
            return False
    return True

def is_LI(M):
    for x in getLocalSemiGroups(M):
        if len(x) > 1:
            return False
    return True
    
def is_QLDA(M):
    S = stable_semigroup(M)
    return is_LDA(S)

def is_QDA(M):
    S = stable_semigroup(M)
    return is_DA(S)

def is_QA(M):
    S = stable_semigroup(M)
    return S.is_Ap()

def is_BG(M):
    for x in M.idempotents():
        for y in M:
            if M.idempotent_power(x+y) != M.idempotent_power(y+x):
                return False
    return True

def is_QSG(M):
    S = stable_semigroup(M)
    return is_SG(S)

def is_QZG(M):
    S = stable_semigroup(M)
    return is_ZG(S)

def is_LG(M):
    for x in getLocalSemiGroups(M):
        if not is_G(x):
            return False
    return True

def is_LZG(M):
    for x in getLocalSemiGroups(M):
        if not is_ZG(x):
            return False
    return True

def is_QLZG(M):
    S = stable_semigroup(M)
    return is_LZG(S)

def AutomataToRegExp(A):
    Initial_states = A._initial_states
    Final_states = A._final_states
    d = A._transitions
    states = A._states
    alphabet = A._alphabet
    Switch = True
    for i in Initial_states:
        for f in Final_states:
            if Switch:
                Swith = False
                L =  Aut2reg(d,alphabet,i,f,states)
            else:
                L = L + Aut2reg(d,alphabet,i,f,states)
    return L                   
def Aut2reg(d,alphabet,i,f,states_list):
    A = alphabet
    states = list(states_list)
    d_reg = {}
    if i in states:
        states.remove(i)
    if f in states:
        states.remove(f)
    for t in d:
        for x in d[t]:
            if (t[0],x) in d_reg:
                d_reg[(t[0],x)] =  d_reg[(t[0],x)] + RegularLanguage(t[1],letters=A) 
            else:
                d_reg[(t[0],x)] =  RegularLanguage(t[1],letters=A)
                   
    while len(states) > 0:
        s = states.pop()
        d_reg_buff = dict(d_reg)
        to_remove = set() 
        for t1 in d_reg_buff:
            for t2 in d_reg_buff:
                if (t1[1] == s) and (t2[0] == s):
                    to_remove.add(t1)
                    to_remove.add(t2)
                    if not (s,s) in d_reg: 
                        if (t1[0],t2[1]) in d_reg:
                            d_reg[(t1[0],t2[1])] =  d_reg[(t1[0],t2[1])] +  d_reg[t1]*d_reg[t2]
                        else:
                            d_reg[(t1[0],t2[1])] =   d_reg[t1]*d_reg[t2]
                    if  (s,s) in d_reg: 
                        if (t1[0],t2[1]) in d_reg:
                            d_reg[(t1[0],t2[1])] =  d_reg[(t1[0],t2[1])] +  d_reg[t1]*(d_reg[(s,s)].kleene_star())*d_reg[t2]
                        else:
                            d_reg[(t1[0],t2[1])] = d_reg[t1]*(d_reg[(s,s)].kleene_star())*d_reg[t2]
        [d_reg.pop(t) for t in to_remove]
    
    if not (i,f) in d_reg:
        raise TypeError("Empty language")
    if (i == f):
        return d_reg[(i,i)].kleene_star()
    if (f,f) in d_reg:
        d_reg[(i,f)] = d_reg[(i,f)]*(d_reg[(f,f)].kleene_star())    
    if (i,i) in d_reg:
        d_reg[(i,f)] = (d_reg[(i,i)].kleene_star())*d_reg[(i,f)]    
    
    if ((f,i) in d_reg):
        return (d_reg[(i,f)]*d_reg[(f,i)]).kleene_star()*d_reg[(i,f)]
    else:
        return d_reg[(i,f)]     

def MonoidToSVG(M,filename,dic):
    file_dot = tempfile.mkdstem(prefix=".",suffix=".dot")[1]
    f = open(file_dot,'w')
    if (dic["Request"] == "SyntacticMonoid"):
        f.write(M.graphviz_string())
 
    if (dic["Request"] == "RightCayley"):
        f.write(M.cayley_graphviz_string(orientation="right"))
    if (dic["Request"] == "LeftCayley"):
        f.write(M.cayley_graphviz_string(orientation="left"))
    f.close()
    os.system('dot -Tsvg %s -o %s'%(file_dot,filename+".svg"))
    
def complete(A):
    A.rename_states()
    d = dict(A._transitions)
    alphabet = A._alphabet
    states = A._states
    sink = len(states)
    complete = True
    for x in states:
        for a in alphabet:
            if not (x,a) in d:
                d[(x,a)] = [sink]
                complete = False
    if not complete:
        for a in alphabet:
            d[(sink,a)] = [sink]
    return Automaton(d,A._initial_states,A._final_states)

def PreImage(M,s):
    if (s == ""):       
        return  emptyAutomaton(M._generators)  
    Words = s.split(",")
    if "1" in Words:
        Words.append("")
        Words.remove("1")
    acc = [monoidElement(x) for x in Words]
    d = {}
    for x in M:
        for a in M._generators:
            d[(x,a)] = [M(x+a)]
    return Automaton(d,[monoidElement("")],acc).minimal_automaton()   

def SubMonoidRequest(M,s):
    Words = s.split(",")
    identity = monoidElement("")
    if "1" in Words:
        Words.append(identity)
        Words.remove("1")
    return SubMonoid(M,Words)
    
def SubMonoid(M,generators):
    G = set(generators)
    G.add(M.get_identity())
    return SubSemiGroup(M,G)

def SubSemiGroup(M,generators):
    d = {}
    letters = [monoidElement((x,)) for x in generators]
    identity = monoidElement("")
    if identity in letters:
        letters.remove(identity)
    toDealWith = list([identity])
    Elements = set()
    if identity in generators:
        Elements.add(identity)
             
    while len(toDealWith)>0:
        y = toDealWith.pop()        
        for a in letters:
            u = y+a[0]

            if len(u)>0:
                z = monoidElement(M(str(u)))
            else:
                z = identity
            if z not in Elements:
                toDealWith.append(z)
                Elements.add(z)
                
            if y in Elements:
                d[(y,a)] = [z]
    A = Automaton(d,[],[],alphabet=letters)
    return TransitionSemiGroup(A) 
      
                                                                   
def QuotientRequest(M,s):
    Words = s.split(",")
    identity = ""
    if "1" in Words:
        Words.append(identity)
        Words.remove("1")
    return Quotient(M,Words)
    
def Quotient(M,C):
    d = {}    
    C = frozenset([monoidElement(u) for u in C])        
    if len(C)<2:
        return M

    for x in M:
        if x not in C:
            d[x] = frozenset([x])
        else:
            d[x] = C                 
    toDealWith = list(d.values())
    while len(toDealWith)>0:
        C = toDealWith.pop()
        for a in M._generators:
            listToMergeR = []
            listToMergeL = []
            for x in C:
                for b in d[a]:
                    xb = M(x+b)
                    if not d[xb] in listToMergeR:
                        listToMergeR.append(d[xb])
                    bx = M(b+x)
                    if not d[bx] in listToMergeL:
                        listToMergeL.append(d[bx])
            DR = frozenset()
            DL = frozenset()
            if len(listToMergeR) > 1:
                for K in listToMergeR:
                    DR = DR.union(K)
            if len(listToMergeL) > 1:
                for K in listToMergeL:
                    DL = DL.union(K)
            if len(DR.intersection(DL))>0:
                D = DR.union(DL)
                toDealWith.append(D)     
                for x in D:
                    d[x] = D
            else:
                if len(DR)>0:
                    toDealWith.append(DR)     
                    for x in DR:
                        d[x] = DR

                if len(DL)>0:
                    toDealWith.append(DL)     
                    for x in DL:
                        d[x] = DL                
            
    transitions = {}
    states = set(d.values())
    for x in states:
        for a in M._generators:
            y = d[M(list(x)[0]+a)]
            transitions[(x,a)] = [y]                         
    A = Automaton(transitions,[],[])
    return TransitionSemiGroup(A)   

       
def makeAutomatonComplete(A):
    A.rename_states()
    d = A._transitions
    for x in A._states:
        for a in A._alphabet:
            if (x,a) not in d:
                d[(x,a)] = [-1]
    for a in A._alphabet:
        d[(bot, a)] = [-1]
    return Automaton(d,A._initial_states,A._final_states)                            

def is_DS(M):
    E = set(M.idempotents())
    All = set(M)
    NonE = All.difference(E)
    for e in E:
        if not (M.is_sub_semigroup(M.J_class_of_element(e))) > 0:
            return False
    return True
def is_G(M):
    E = set(M.idempotents())    
    if len(E) > 1:
        return False
    else:
        return True
def is_Gsol(M):
    if is_G(M):                                
        return is_solvable(M)
    else:
        return False    

def is_ZG(M):
    done = set()
    for x in M:
        x = M(M.idempotent_power(x) + x)
        if x in done:
            continue
        done.add(x)
        for y in M:
            if not M(x+y) == M(y+x):
                return False
    return True

def is_SG(M):
    done = set()
    for x in M:
        e = M.idempotent_power(x)
        x = M(e + x)
        if x in done:
            continue
        done.add(x)
        for y in M:
            if not M(x+y+e) == M(e+y+x):
                return False
    return True

            
def is_Msol(M):
    E = set(M.idempotents())
    while len(E) > 0:
        e = E.pop()
        J = M.J_class_of_element(e)
        E = E.difference(J)
        H = HclassToGroup(M,e)
        if not is_solvable(H):
            return False
    return True        

def is_SL(M):
    return (M.is_Idempotent() and M.is_Commutative()) 

def is_J(M):
    E = M.idempotents()
    for e in E:
        if len(M.J_class_of_element(e))>1:
            return False
    return True    
def is_R(M):
    E = M.idempotents()
    for e in E:
        if len(M.R_class_of_element(e))>1:
            return False
    return True    
def is_L(M):
    E = M.idempotents()
    for e in E:
        if len(M.L_class_of_element(e))>1:
            return False
    return True  
def is_Nil(M):
    if is_J(M):
        return (len(M.idempotents()) <= 2) 
    else:
        return False      
def is_DA(M):
    return (M.is_Ap() and is_DS(M))                         

def is_Jplus(M, reverse=False):
    if len(M) == 1:
        return True
    A = M._automaton
    O = nx.transitive_closure(nx.DiGraph(list(computeTransitionOrderedMonoid(A))))        
    identity = monoidElement("")
    if reverse:
        reverse = lambda x:(x[1],x[0])
    else:
        reverse = lambda x:x
    for x in M:
        if x == identity:
            continue
        if not reverse((x ,identity)) in O.edges():
            return False 
    return True

def is_JplusUn(M, reverse=False):
    A = M._automaton
    N = TransitionSemiGroup(A, monoid=False)
    O = nx.transitive_closure(nx.DiGraph(list(computeTransitionOrderedMonoid(A))))        
    if reverse:
        reverse = lambda x:(x[1],x[0])
    else:
        reverse = lambda x:x
    for e in N.idempotents():
        for x in N:
            a = N(e+x+e)
            if a != e and not reverse((a, e)) in O.edges():
                return False
    return True 

def is_Tract_RPQ_simple(M):
    if not is_JplusUn(M, reverse=True):
        return False
    A = M._automaton
    N = TransitionSemiGroup(A, monoid=False)
    O = nx.transitive_closure(nx.DiGraph(list(computeTransitionOrderedMonoid(A))))        
    edges = set(O.edges())
    edges.update([(x,x) for x in N])
    E = N.idempotents()
    for e in E:
        for f in E:
            ef = N(e+f)
            for z in N:
                ezf = N(e+z+f)
                if not (ef, ezf) in edges:
                    return False
    return True

def is_Tract_RPQ_trail(M):
    if not is_JplusUn(M, reverse=True):
        return False
    A = M._automaton
    N = TransitionSemiGroup(A, monoid=False)
    O = nx.transitive_closure(nx.DiGraph(list(computeTransitionOrderedMonoid(A))))        
    edges = set(O.edges())
    edges.update([(x,x) for x in N])
    for x in N:
        for y in N:
            e = N.idempotent_power(N(x+y))
            for z in N:
                f = N.idempotent_power(N(x+z))
                for u in N:
                    if not (N(e+f), N(e+u+f)) in edges:
                        return False
    return True
    I = set(N.idempotents())
    R_max = {}
    S = set(N)
    while S:
        x = N.pop_J_maximal(S)
        R_max[x] = U = set()
        for y in N:
            f = N(x+y)
            if f in I:
                U.add(f)
            S.discard(y)
    pairs = set()
    for U in R_max.values():
        for e in U:
            for f in U:
                if (e,f) in pairs:
                    continue
                pairs.add((e,f))
                ef = N(e+f)
                for z in N:
                    ezf = N(e+z+f)
                    if not (ef, ezf) in O.edges():
                        return False
    return True 
     
    
            
def HclassToGroup(M,x):
    H = list(M.H_class_of_element(x))
    try:
        e = M.idempotents().intersection(H).pop()
    except:
        raise ValueError("The H-class of %s must contain an idempotent",str(x))
    d = {}    
    for x in H:
        for y in H:
            d[(x,(y,))] = [M(x+y)]
    return  TransitionSemiGroup(Automaton(d,[e],[e]), monoid=False)

def compute_inverse(H):
    H.inverse = {}
    for x in H:
        H.inverse[x] = H._Representations[H._Representations_rev[x].inverse()]               
def commutators_subgroup(H,G,verbose=False):
    C = [G(x+y+G.inverse[x]+G.inverse[y]) for x in H for y in H]
    if verbose:
        print(len(C))
    H = set(G.sub_monoid_generated(C)._automaton._states)
    return H
    
def is_solvable(H):
    compute_inverse(H)
    K = set(H)
    CK = commutators_subgroup(K,H)
    while len(CK) < len(K):
        K = CK
        CK = commutators_subgroup(K,H)
    if len(CK) == 1:
        return True
    else:
        return False
     
def check_order_pairs_inclusion(E,O):
    for x in E:
        K = list(E[x])
        K.remove(x)
        for y in K:
            if (x,y) not in O.edges():
                return False
    return True
    
def sigma_level(M):    
    A = M._automaton
    O = nx.transitive_closure(nx.DiGraph(list(computeTransitionOrderedMonoid(A))))        
    Pairs = compute_pairs(M)
    tight = test_tighness(M, pairs=Pairs) 
    s = 0
    while not check_order_pairs_inclusion(Pairs[s][1],O):
        s += 1

    p = 0
    O = O.reverse()
    while not check_order_pairs_inclusion(Pairs[p][1],O):
        p += 1
    return (s, p, tight)
    
def permutation_group(n):
    d = {}
    for i in range(n):
        d[(i,"a")] = [(i+1)%n]
        d[(i,"b")] = [i]
    d[(0,"b")] = [1]
    d[(1,"b")] = [0]
    return TransitionSemiGroup(Automaton(d,[0],[0]))

def HclassToGapGroup(M,x):
    H = set(M.H_class_of_element(x))
    E = set(M.idempotents())
    H = list(H)
    n = len(H)
    permutations = []
    for x in H: 
        permx = []           
        Rem = set(range(1,n+1))
        while len(Rem) > 0:
            i = Rem.pop()            
            j = H.index(M(H[i-1]+x))+1                
            cyclei = (i,)
            while not (j == i):
                Rem.remove(j)
                cyclei += (j,)
                j = H.index(M(H[j-1]+x))+1
            if len(cyclei) > 1:    
                permx.append(cyclei)
        if len(permx) > 0:
            permutations.append(permx)              
    G = SymmetricGroup(n)
    return G.subgroup(permutations)
pi = "Π"
sig = "Σ"

VarietyLattice = {
    "All":["Msol","W", "dual(W)", "BG"],
    "dual(W)": [ "DS", f"{pi}2"], 
    "W":["DS", f"{sig}2"],
    "BG":["DS"],
    "Msol":["Gsol", "QSG"],
    "QSG" : ["SG", "QLZG", "QA"],
    "QLZG": ["QZG", "LZG"],
    "QZG": ["ZG"],
    "LZG": ["LG", "ZG"],
    "LG" : ["G", "LI"],
    "QA":["Ap", "QLDA"], 
    "QLDA":["QDA", "LDA"], 
    "SG":["Ap", "ZG"], 
    "ZG" : ["Com", "Nil"], 
    "Ap":["MeDA", f"{sig}2", f"{pi}2"], 
    "MeDA":["LDA"],
    f"{sig}2":["DA"], 
    f"{pi}2":["DA"], 
    "LDA":["DA", "LJ"], 
    "QDA":["DA"], 
    "DS":["G","DA","Com"],
    "Com":["Ab","ACom"],
    "G":["Gsol"],
    "Gsol":["Ab"],
    "DA":["Idempotent","R","L", "DA&LJ"],
    "R":["J"],
    "L":["J"],
    "LJ":["LSL", "DA&LJ", "Σ1[<, +1]", f"{pi}1[<, +1]"], 
    "LSL": ["LI", "SL"],
    "DA&LJ":["J"], 
    "J":["Σ1[<]",f"{pi}1[<]", "Nil"],
    "ACom":["SL"],
    "Σ1[<, +1]": ["Σ1[<]"],
    f"{pi}1[<, +1]":["Tract_RPQ_trail"],
    "Tract_RPQ_trail": ["Tract_RPQ_simple"],
    "Tract_RPQ_simple": [f"{pi}1[<]"],
    "Σ1[<]": ["Trivial"],
    f"{pi}1[<]": ["Trivial"],
    "Idempotent":["SL"],
    "Nil":["SL"],
    "SL":["Trivial"],
    "Ab":["Trivial"],
    "LI": ["Trivial"],
    "Trivial":[]
}
VarietyList = VarietyLattice.keys()
LogicLattice = {"MSO[<]":["FO+MOD[<]"],"FO+MOD[<]":["MOD[<,+1]","FO[<]","FO2[<,+1,Mod]"],"MOD[<,+1]":["MOD[<]","MOD[+1]"],"MOD[<]":["MOD[]"],"MOD[+1]":["MOD[]"],"MOD[]":["Trivial"],"FO[<]":["FO2[<,+1]"],"FO2[<,+1]":["FO2[<],FO[+1]"],"FO2[<,+1,Mod]":["FO2[<,+1]","FO2[<,Mod]","FO[+1,Mod]"],"FO2[<,Mod]":["FO2[<]"],"FO[+1,Mod]":["FO[MOD]","FO[+1]"],"FO2[<]":["BΣ1[<]"]}

def VertexGraphvizLattice(Lattice,point,LatticeColor,done):
    if point in done:
        return ""
    else:    
        done.add(point)
        s = '"'+point+'" [fillcolor='+LatticeColor[point]+'];\n'
        for x in Lattice.get(point, ()):
            s += VertexGraphvizLattice(Lattice,x,LatticeColor,done)
        return s
def EdgeGraphvizLattice(Lattice,point,done):
    s = ""
    if not (point in done):
        done.add(point) 
        for x in Lattice.get(point, ()):
            s += '"'+point+'" -> "'+x+'";\n'                
            s += EdgeGraphvizLattice(Lattice,x,done)
    return s           
def GraphvizLattice(Lattice,point,LatticeColor):
    doneVertex = set()
    doneEdge = set()
    return 'digraph {\nnode [style=filled]\n'+ VertexGraphvizLattice(Lattice,point,LatticeColor,doneVertex) + EdgeGraphvizLattice(Lattice,point,doneEdge)+'}'

def GraphvizVariety(M):
    LatticeColor = collections.defaultdict(lambda:"yellow")
    VarLattice = copy.deepcopy(VarietyLattice)
    for x in VarietyList:
        try:
            if VarietyTest(M,x):
                LatticeColor[x] = "green"
            else:
                LatticeColor[x] = "lightgrey"
        except Exception as inst:
            LatticeColor[x] = "firebrick1"
            print("error:", inst, inst.args)
    if M.is_Ap():
        s, p, tight = sigma_level(M)
        print(f"sigma_level {s=}, {p=}, {tight=}")
        if tight:
            delta = max(s, p)
            deltac = "DA" if delta == 2 else f"𝚫{delta}"
            LatticeColor[deltac] = "green"
            if s > 0 and p > 0:
                VarLattice["Ap"].extend([f"{sig}{delta}", f"{pi}{delta}"])
                VarLattice[f"{sig}{delta}"] = VarLattice[f"{pi}{delta}"] = [deltac]
                LatticeColor[f"{sig}{delta}"] = LatticeColor[f"{pi}{delta}"] = "green"
                LatticeColor[deltac] = "green"
                LatticeColor[f"{sig}{s}"] = "green"
                LatticeColor[f"{pi}{p}"] = "green"
                if deltac != "DA" and delta > 0:
                    VarLattice[deltac] = [f"{sig}{delta-1}", f"{pi}{delta-1}"]
                    VarLattice["{sig}{delta-1}"] = VarLattice[f"{pi}{delta-1}"] = ["DA"]
                if delta > 2:
                    deltacc = "DA" if delta == 3 else f"𝚫{delta-1}"
                    VarLattice[f"{sig}{delta-1}"] = VarLattice[f"{pi}{delta-1}"] = [deltacc]
                    LatticeColor[deltacc] = "lightgrey"
                    LatticeColor[f"{sig}{delta-1}"] = "lightgrey" if s > delta -1 else "green"
                    LatticeColor[f"{pi}{delta-1}"] = "lightgrey" if p > delta -1 else "green"
                    if deltacc != "DA":
                        VarLattice[deltacc] = ["DA"]
                        VarLattice[deltacc] = [f"{sig}2", f"{pi}2"]
            else:
                LatticeColor[f"{sig}2"] = "green"
                LatticeColor[f"{pi}2"] = "green"
                    
        else:
            raise ValueError("Not a tight monoid !!!!")

    return GraphvizLattice(VarLattice,"All",LatticeColor)                         

def VarietyListToSVG(M,filename):
    file_dot = tempfile.mkstemp(prefix=".",suffix=".dot")[1]
    f = open(file_dot,'w')
    f.write(GraphvizVariety(M))
    f.close()
    os.system('dot -Tsvg %s -o %s'%(file_dot,filename+".svg"))

def VarietyTest(M,VarietyName):
    if (VarietyName == "All"):
        return True
    if (VarietyName == "BG"):
        return is_BG(M)
    if (VarietyName == "Trivial"):
        return (len(M) == 1)
    if (VarietyName == "Ap"):
        return M.is_Ap()
    if (VarietyName == "QA"):
        return is_QA(M)
    if (VarietyName == "QLZG"):
        return is_QLZG(M)
    if (VarietyName == "QSG"):
        return is_QSG(M)
    if (VarietyName == "QZG"):
        return is_QZG(M)
    if (VarietyName == "LZG"):
        return is_LZG(M)
    if (VarietyName == "LG"):
        return is_LG(M)
    if (VarietyName == "QLDA"):
        return is_QLDA(M)
    if (VarietyName == "LDA"):
        return is_LDA(M)
    if (VarietyName == "MeDA"):
        return is_MeDA(M)
    if (VarietyName == "QDA"):
        return is_QDA(M)
    if (VarietyName == "J"):
        return is_J(M)
    if (VarietyName == "Nil"):
        return is_Nil(M)
    if (VarietyName == "R"):
        return is_R(M)
    if (VarietyName == "L"):
        return is_L(M)
    if (VarietyName == "LJ"):
        return is_LJ(M)
    if (VarietyName == "LI"):
        return is_LI(M)
    if (VarietyName == "Tract_RPQ_trail"):
        return is_Tract_RPQ_trail(M)
    if (VarietyName == "Tract_RPQ_simple"):
        return is_Tract_RPQ_simple(M)
    if (VarietyName == "DA&LJ"):
        return is_LJ(M) and is_DA(M)
    if (VarietyName == "DS"):
        return is_DS(M)
    if (VarietyName == "G"):
        return is_G(M)
    if (VarietyName == "Gsol"):
        return is_Gsol(M)
    if (VarietyName == "Msol"):
        return is_Msol(M)
    if (VarietyName == "W"):
        return is_W(M)
    if (VarietyName == "dual(W)"):
        return is_dual_W(M)
    if (VarietyName == "DA"):
        return is_DA(M)             
    if (VarietyName == "Com"):
        return (M.is_Commutative()) 
    if (VarietyName == "ACom"):
        return (M.is_Commutative() and M.is_Ap()) 
    if (VarietyName == "Ab"):
        return (M.is_Commutative() and is_G(M)) 
    if (VarietyName == "Idempotent"):
        return (M.is_Idempotent()) 
    if (VarietyName == "SL"):
        return is_SL(M)
    if (VarietyName == "LSL"):
        return is_LSL(M)
    if (VarietyName == "SG"):
        return is_SG(M)
    if (VarietyName == "ZG"):
        return is_ZG(M)
    if (VarietyName == "Σ1[<]"):
        return is_Jplus(M)
    if (VarietyName == "Σ1[<, +1]"):
        return is_JplusUn(M)
    if (VarietyName == f"{pi}1[<]"):
        return is_Jplus(M, reverse=True)
    if (VarietyName == f"{pi}1[<, +1]"):
        return is_JplusUn(M, reverse=True)
