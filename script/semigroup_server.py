import multiprocessing
import time
from socketserver import ThreadingMixIn
from http.server import SimpleHTTPRequestHandler, HTTPServer
import threading

import sys
from tool import *
import urllib 
local = "localhost"
web = 'paperman.name'

HOST_NAME = local    # !!!REMEMBER TO CHANGE THIS!!!
PORT_NUMBER = 8001 # Maybe set this to 9000.
date = time.asctime().replace(" ","").replace(":",".")
log = open("log/"+date,"w")

class MyHandler(SimpleHTTPRequestHandler):
    def do_HEAD(s):
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
    def do_GET(s):
        """Respond to a GET request."""
        s.send_response(200)
        s.send_header("Content-type", "text/html")
        s.end_headers()
        s.wfile.write(deal_request(s.path).encode())
        log.write(s.path)
        print(s.path)
class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""        
 
if __name__ == '__main__':
    httpd =  ThreadedHTTPServer((HOST_NAME, PORT_NUMBER), MyHandler)
    print(time.asctime(), "Server Starts - %s:%s" % (HOST_NAME, PORT_NUMBER))
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), "Server Stops - %s:%s" % (HOST_NAME, PORT_NUMBER))

